<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    function index()
    {
        $json = file_get_contents(storage_path('products.json'));
        $products = json_decode($json, true);

        return response()->json($products);
    }

    function store(Request $request)
    {
        $json = file_get_contents(storage_path('products.json'));
        $products = json_decode($json, true);
        $product = [
            'name' => $request->get('name'),
            'stock_qty' => $request->get('stock_qty'),
            'price' => $request->get('price'),
            'created_at' => date("Y-m-d H:i:s"),
        ];

        array_push($products, $product);
        file_put_contents(storage_path('products.json'), json_encode($products));

        return response()->json($product);
    }

    function update()
    {
        //
    }
}
