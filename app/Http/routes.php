<?php

$router->get('/', function () {
    return view('index');
});

$router->group(['prefix' => 'api', 'namespace' => 'Api', 'middleware' => ['api']], function ($router) {
    $router->resource('products', 'ProductsController', ['only' => ['index', 'store', 'update']]);
});
