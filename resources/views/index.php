<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Coalition Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/paper/bootstrap.min.css" rel="stylesheet" integrity="sha384-2mX2PSpkRSXLQzmNzH3gwK6srb06+OfbDlYjbog8LQuALYJjuQ3+Yzy2JIWNV9rW" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        <style>
            .product-form {margin: 30px 0;}
            .product-list {margin: 30px 0;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div id="app">
                    <div class="product-form">
                        <h3>Add Product</h3>
                        <form action="" method="POST" role="form" @submit.prevent="submit">
                            <div class="form-group">
                                <label for="name">Product name</label>
                                <input type="text" name="name" id="name" v-model="new_product.name" required>
                            </div>
                            <div class="form-group">
                                <label for="stock_qty">Quantity in Stock</label>
                                <input type="number" name="stock_qty" id="stock_qty" v-model="new_product.stock_qty" min="0" step="1" required>
                            </div>
                            <div class="form-group">
                                <label for="price">Price per item</label>
                                <input type="number" name="price" id="price" v-model="new_product.price" min="0" step="0.5" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-lg">Add</button>
                            </div>
                        </form>
                    </div>
                    <div class="product-list">
                        <h3>All Products</h3>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Submitted</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="product in products">
                                <td>{{ product.name }}</td>
                                <td>{{ product.stock_qty }}</td>
                                <td>{{ product.price }}</td>
                                <td>{{ product.created_at }}</td>
                                <td>${{ product.stock_qty * product.price }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.3/vue-resource.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script>
            new Vue({
                el: '#app',
                data: {
                    new_product: {},
                    products: []
                },
                computed: {
                    total: function() {
                        return this.new_product.stock_qty * this.new_product.price;
                    }
                },
                ready: function() {
                    this.fetchProducts();
                },
                methods: {
                    submit: function() {
                        this.$http.post('/api/products', this.new_product).then(function (response) {
                            this.products.push(response.data);
                            this.new_product = {};
                        });
                    },
                    fetchProducts: function() {
                        this.$http.get('/api/products').then(function (response) {
                            this.products = response.data;
                        });
                    }
                }
            });
        </script>
    </body>
</html>
